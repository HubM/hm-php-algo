<?php

for($i = 1; $i < 4; $i++) {
	$input = explode(PHP_EOL, file_get_contents('cartes_partie_'.$i.'.txt'));
	echo "<h2>Jeu " . $i . " avec " . $input[0] . " tours</h2>";

	unset($input[0]);

	$comptA = 0;
	$comptB = 0;

	foreach ($input as $key) {
		$values = explode(' ',$key);

		if($values[0] > $values[1]) {
			$comptA++;
		} elseif ($values[1] > $values[0]) {
			$comptB++;
		} else {
			$comptA++;
			$comptB++;
		}
	}

	if($comptA > $comptB) {
		echo "Le joueur <b>A</b> l'emporte avec <b>" .$comptA. "</b> points contre <b>" .$comptB. "</b> points pour le joueur <b>B</b>";
	} else {
		echo "Le joueur <b>B</b> l'emporte avec <b>" .$comptB. "</b> points contre <b>" .$comptA. "</b> points pour le joueur <b>A</b>";
	}
}
