<?php

for($i = 1; $i < 4; $i++) {
	$input = explode(PHP_EOL, file_get_contents('input'.$i.'.txt'));

	$product_target = $input[1];
	echo "<h1>Produit cible : " .$product_target. " - ".$input[0]. " produits</h1><br/>";

	unset($input[0]);
	unset($input[1]);

	$min_price = 10000;
	foreach ($input as $key) {
		$values = explode(' ',$key);

		$product_name = $values[0];
		$product_price = $values[1];


		if($product_name == $product_target && $min_price > $product_price) {
			$min_price = $product_price;
		}
	}
	echo "Le prix minimal pour le produit <b>" .$product_target. "</b> est de <b>" .$min_price. "€</b>";
}
